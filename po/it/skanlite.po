# translation of skanlite.po to Italian
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Valter Mura <valtermura@gmail.com>, 2008, 2009, 2016, 2017.
# Federico Zenith <federico.zenith@member.fsf.org>, 2009, 2010, 2011, 2012, 2013, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: skanlite\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2018-01-13 03:06+0100\n"
"PO-Revision-Date: 2017-03-29 21:23+0200\n"
"Last-Translator: Valter Mura <valtermura@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 1.5\n"

#: ImageViewer.cpp:57
#, kde-format
msgid "Zoom In"
msgstr "Ingrandisci"

#: ImageViewer.cpp:60
#, kde-format
msgid "Zoom Out"
msgstr "Rimpicciolisci"

#: ImageViewer.cpp:63
#, kde-format
msgid "Zoom to Actual size"
msgstr "Dimensioni reali"

#: ImageViewer.cpp:66
#, kde-format
msgid "Zoom to Fit"
msgstr "Adatta"

#: main.cpp:46
#, kde-format
msgid "Skanlite"
msgstr "Skanlite"

#: main.cpp:48
#, kde-format
msgid "Scanning application by KDE based on libksane."
msgstr "Applicazione di KDE per la scansione basata su libksane."

#: main.cpp:50
#, kde-format
msgid "(C) 2008-2016 Kåre Särs"
msgstr "© 2008-2016 di Kåre Särs"

#: main.cpp:55
#, kde-format
msgid "Kåre Särs"
msgstr "Kåre Särs"

#: main.cpp:56
#, kde-format
msgid "developer"
msgstr "Sviluppatore"

#: main.cpp:59
#, kde-format
msgid "Gregor Mi"
msgstr "Gregor Mi"

#: main.cpp:60 main.cpp:63
#, kde-format
msgid "contributor"
msgstr "Collaboratore"

#: main.cpp:62
#, kde-format
msgid "Arseniy Lartsev"
msgstr "Arseniy Lartsev"

#: main.cpp:65
#, kde-format
msgid "Gilles Caulier"
msgstr "Gilles Caulier"

#: main.cpp:66 main.cpp:72
#, kde-format
msgid "Importing libksane to extragear"
msgstr "Importazione di libksane in extragear"

#: main.cpp:68
#, kde-format
msgid "Anne-Marie Mahfouf"
msgstr "Anne-Marie Mahfouf"

#: main.cpp:69
#, kde-format
msgid "Writing the user manual"
msgstr "Scrittura del manuale utente"

#: main.cpp:71
#, kde-format
msgid "Laurent Montel"
msgstr "Laurent Montel"

#: main.cpp:74
#, kde-format
msgid "Chusslove Illich"
msgstr "Chusslove Illich"

#: main.cpp:75 main.cpp:78
#, kde-format
msgid "Help with translations"
msgstr "Aiuto con le traduzioni"

#: main.cpp:77
#, kde-format
msgid "Albert Astals Cid"
msgstr "Albert Astals Cid"

#: main.cpp:81
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Valter Mura,Federico Zenith"

#: main.cpp:82
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "valtermura@gmail.com,"

#: main.cpp:91
#, kde-format
msgid "Sane scanner device name. Use 'test' for test device."
msgstr "Nome dispositivo scanner Sane. Usa «test» per provare il dispositivo."

#: main.cpp:91
#, kde-format
msgid "device"
msgstr "dispositivo"

#. i18n: ectx: property (windowTitle), widget (QDialog, SaveLocation)
#: SaveLocation.ui:20
#, kde-format
msgid "Save Location"
msgstr "Dove salvare"

#. i18n: ectx: property (text), widget (QLabel, locationLabel)
#. i18n: ectx: property (text), widget (QLabel, label)
#: SaveLocation.ui:28 settings.ui:80
#, kde-format
msgid "Save Location:"
msgstr "Dove salvare:"

#. i18n: ectx: property (text), widget (QLabel, namePrefixLabel)
#: SaveLocation.ui:52
#, kde-format
msgid "Name & Format:"
msgstr "Nome e formato:"

#. i18n: ectx: property (text), widget (QLabel, numberLabel)
#. i18n: ectx: property (text), widget (QLabel, label_3)
#: SaveLocation.ui:71 settings.ui:131
#, kde-format
msgid "####."
msgstr "####."

#. i18n: ectx: property (text), widget (QLabel, numStartFromLabel)
#: SaveLocation.ui:83
#, kde-format
msgid "Start numbering from:"
msgstr "Avvia la numerazione da:"

#. i18n: ectx: property (text), widget (QLabel, u_resultLabel)
#: SaveLocation.ui:120
#, kde-format
msgid "Example:"
msgstr "Esempio:"

#. i18n: ectx: property (text), widget (QLabel, u_resultValue)
#: SaveLocation.ui:127
#, kde-format
msgid "/home/joe/example.png"
msgstr "/home/pippo/esempio.png"

#. i18n: ectx: property (windowTitle), widget (QWidget, SkanliteSettings)
#: settings.ui:14 skanlite.cpp:69
#, kde-format
msgid "Settings"
msgstr "Impostazioni"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: settings.ui:20
#, kde-format
msgid "Image saving"
msgstr "Salvataggio dell'immagine"

#. i18n: ectx: property (text), widget (QLabel, label_7)
#: settings.ui:26
#, kde-format
msgid "Preview before saving:"
msgstr "Anteprima prima di salvare:"

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: settings.ui:46
#, kde-format
msgid "Save mode:"
msgstr "Come salvare:"

#. i18n: ectx: property (text), item, widget (QComboBox, saveModeCB)
#: settings.ui:60
#, kde-format
msgid "Open the save dialog for every image"
msgstr "Apri la finestra di salvataggio per ciascuna immagine"

#. i18n: ectx: property (text), item, widget (QComboBox, saveModeCB)
#: settings.ui:65
#, kde-format
msgid "Open the save dialog for the first image only"
msgstr "Apri la finestra di salvataggio solo per prima immagine"

#. i18n: ectx: property (text), widget (QPushButton, getDirButton)
#: settings.ui:102
#, kde-format
msgid "..."
msgstr "..."

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: settings.ui:109
#, kde-format
msgid "Name && Format:"
msgstr "Nome &e formato:"

#. i18n: ectx: property (text), widget (QLineEdit, imgPrefix)
#: settings.ui:124
#, kde-format
msgid "Image"
msgstr "Immagine"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: settings.ui:143
#, kde-format
msgid "Specify save quality:"
msgstr "Specifica la qualità di salvataggio:"

#. i18n: ectx: property (suffix), widget (QSpinBox, imgQuality)
#: settings.ui:202
#, no-c-format, kde-format
msgid "%"
msgstr "%"

#. i18n: ectx: property (title), widget (QGroupBox, generalGB)
#: settings.ui:230
#, kde-format
msgid "General"
msgstr "Generale"

#. i18n: ectx: property (text), widget (QCheckBox, setPreviewDPI)
#: settings.ui:236
#, kde-format
msgid "Set preview resolution (DPI)"
msgstr "Imposta risoluzione di anteprima (DPI)"

#. i18n: ectx: property (text), item, widget (QComboBox, previewDPI)
#: settings.ui:250
#, kde-format
msgid "50"
msgstr "50"

#. i18n: ectx: property (text), item, widget (QComboBox, previewDPI)
#: settings.ui:255
#, kde-format
msgid "75"
msgstr "75"

#. i18n: ectx: property (text), item, widget (QComboBox, previewDPI)
#: settings.ui:260
#, kde-format
msgid "100"
msgstr "100"

#. i18n: ectx: property (text), item, widget (QComboBox, previewDPI)
#: settings.ui:265
#, kde-format
msgid "150"
msgstr "150"

#. i18n: ectx: property (text), item, widget (QComboBox, previewDPI)
#: settings.ui:270
#, kde-format
msgid "300"
msgstr "300"

#. i18n: ectx: property (text), widget (QCheckBox, u_disableSelections)
#: settings.ui:283
#, kde-format
msgid "Disable automatic selections"
msgstr "Disattiva le selezioni automatiche"

#. i18n: ectx: property (text), widget (QPushButton, revertOptions)
#: settings.ui:316
#, kde-format
msgid "Revert scanner options to default values"
msgstr "Riporta le opzioni dello scanner ai valori predefiniti"

#: skanlite.cpp:67
#, kde-format
msgid "About"
msgstr "Informazioni"

#: skanlite.cpp:157
#, kde-format
msgid "Skanlite Settings"
msgstr "Impostazioni di Skanlite"

#: skanlite.cpp:178
#, kde-format
msgid "Opening the selected scanner failed."
msgstr "Apertura dello scanner selezionato non riuscita."

#: skanlite.cpp:182
#, kde-format
msgctxt "@title:window %1 = scanner maker, %2 = scanner model"
msgid "%1 %2 - Skanlite"
msgstr "%1 %2 - Skanlite"

#: skanlite.cpp:187
#, kde-format
msgctxt "@title:window %1 = scanner device"
msgid "%1 - Skanlite"
msgstr "%1 - Skanlite"

#: skanlite.cpp:279
#, kde-format
msgid "%1: %2"
msgstr "%1: %2"

#: skanlite.cpp:299
#, kde-format
msgctxt "prefix for auto naming"
msgid "Image-"
msgstr "Immagine-"

#: skanlite.cpp:412
#, kde-format
msgid ""
"The image will be saved in the PNG format, as Skanlite only supports saving "
"16 bit color images in the PNG format."
msgstr ""
"L'immagine verrà salvata nel formato PNG, perché Skanlite supporta solo il "
"salvataggio di immagini a colori a 16 bit nel formato PNG."

#: skanlite.cpp:445
#, kde-format
msgid "New Image File Name"
msgstr "Nuovo nome del file immagine"

#: skanlite.cpp:479
#, kde-format
msgid "Do you want to overwrite \"%1\"?"
msgstr "Vuoi sovrascrivere «%1»?"

#: skanlite.cpp:534 skanlite.cpp:547
#, kde-format
msgid "Failed to save image"
msgstr "Salvataggio dell'immagine non riuscito"

#: skanlite.cpp:561
#, kde-format
msgid "Failed to upload image"
msgstr "Invio dell'immagine non riuscito"